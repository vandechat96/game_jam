﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneFinish : MonoBehaviour
{
    
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnCollisionEnter(Collision other)//regarde quelle piece a toucher la fin du niveau et charge la bonne zone en consequance 
    {
        if (other.gameObject.CompareTag("1cent"))
        {
            Application.LoadLevel("Niveau2");
        }
        if (other.gameObject.CompareTag("10cent"))
        {
            Application.LoadLevel("Niveau3");
        }
        if (other.gameObject.CompareTag("50cent"))
        {
            Application.LoadLevel("Niveau4");
        }
        if (other.gameObject.CompareTag("1dollar"))
        {
            Application.LoadLevel("Niveau5");
        }
        if (other.gameObject.CompareTag("5dollar"))
        {
            Application.LoadLevel("Niveau6");
        }
        if (other.gameObject.CompareTag("10dollar"))
        {
            Application.LoadLevel("Niveau7");
        }
        if (other.gameObject.CompareTag("20dollar"))
        {
            Application.LoadLevel("Niveau8");
        }
        if (other.gameObject.CompareTag("50dollar"))
        {
            Application.LoadLevel("Niveau9");
        }
        if (other.gameObject.CompareTag("100dollar"))
        {
            Application.LoadLevel("Niveau10");
        }
        if (other.gameObject.CompareTag("cartbanquaire"))
        {
            Application.LoadLevel("Niveau11");
        }
        if (other.gameObject.CompareTag("humain"))
        {
            Application.LoadLevel("Niveau1");
        }

    }
}
