﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deplacementBillet : MonoBehaviour {
    public GameObject billet;
    private Rigidbody Prigidbody;
    public float Speed;
    public int distancesol;//distance du raycast
    public Transform spawn;
    public bool IsGrounded = false;
    public GameObject TexteDepar;
    public GameObject TextFin;
    public Animator Anime;
    public float time = 0;
    public bool timer = true;

    // Use this for initialization
    void Start () {
        Prigidbody = GetComponent<Rigidbody>();
        TexteDepar.SetActive(true);
        Anime = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update () {

        if (timer == true)
        {
            time = time + Time.deltaTime;
        }

        if (time > 10)//gestion texte
        {
            TextFin.SetActive(true);
            time = 0;
            timer = false;
        }

        Anime.enabled = false;//arrete  l'animation de marche
        IsGrounded = Isgrounded();//variable = fonction
        if (Isgrounded() == true)
        {
            if (Input.GetKeyDown(KeyCode.Space))//saut
            {             
                Vector3 saut = new Vector3(0.0f, 1000, 0.0f);
                Prigidbody.AddForce(saut * 30 * Time.deltaTime);
            }
        }
        if (Input.GetKey(KeyCode.O))
        {
            billet.transform.rotation = Quaternion.Euler(0, 90, 0);//remet le joueur droit
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            Anime.enabled = true;//lance l'animation de marche
            billet.transform.position = new Vector3(billet.transform.position.x + Speed, billet.transform.position.y, billet.transform.position.z);//déplacement
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Anime.enabled = true;//lance l'animation de marche
            
            billet.transform.position = new Vector3(billet.transform.position.x + -Speed, billet.transform.position.y, billet.transform.position.z);//déplacement
        }
        
    }
    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("ZoneMort"))
        {//quand on meurt
            billet.transform.position = new Vector3(spawn.position.x, spawn.position.y, spawn.position.z);//on est replacé au point de départ
            billet.transform.rotation = Quaternion.Euler(0, 90, 0);           
        }
    }
    public bool Isgrounded()//fonction pour voir si lejoueur  es au sol
    {
        Debug.DrawRay(transform.position, (-transform.up) * distancesol, Color.green);//permet de voir le raycast              
        return Physics.Raycast(transform.position, -transform.up, distancesol);//raycast pour savoir si le joueur touche le sole
    }
}
