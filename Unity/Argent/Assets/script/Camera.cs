﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

    public GameObject joueur;       
    public Vector3 offset;         

    // Initialisation
    void Start()
    {       
        offset = transform.position - joueur.transform.position;//calcule 
    }

    
    void LateUpdate()
    {
        //position cam + le décalage calculer au dessu
        transform.position = joueur.transform.position + offset;
    }
}
