﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeplacementPiece : MonoBehaviour {
    public GameObject Piece;
    public GameObject cameraaaa;
    //public Transform test;
    public int Speed;
    public Transform spawn;
    public int HauterSaut;
    public int MaxSpeed;
    public int MinSpeed;
    private Rigidbody Prigidbody;
    public bool IsGrounded = false;
    public GameObject TexteDepar;
    public GameObject TextFin;
    public float time = 0;
    public bool timer = true;

    

    // Use this for initialization
    void Start () {
        Prigidbody = GetComponent<Rigidbody>();
        
        TexteDepar.SetActive(true);//gestion des textes

    }
	
	// Update is called once per frame
	void Update () {
        //déplacement
        float moveHorizontal = Input.GetAxis("Horizontal");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, 0.0f);//déplacement latéral
        Prigidbody.AddForce(movement * Speed * Time.deltaTime);
        if (timer == true)
        {
            time = time + Time.deltaTime;
        }      
        if(time > 10)
        {//gestion des textes
            TextFin.SetActive(true);
            time = 0;
            timer = false;
        }   
        if (Input.GetKeyDown(KeyCode.Space)&& IsGrounded == true)//saut
        {        
            Vector3 saut = new Vector3(0.0f, 1000, 0.0f);
            Prigidbody.AddForce(saut * 30 * Time.deltaTime);                     
        }

    }
    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Ground"))//verif si le joueur touche le sol (pour le saut)
        {
            IsGrounded = true;
            Debug.Log("est au sol");
        }
        if (other.gameObject.CompareTag("ZoneMort"))
        {
            Piece.transform.position = new Vector3(spawn.position.x, spawn.position.y, spawn.position.z);//si le joueur tombe on le remet au pôint de dépars       
        }
    }
    public void OnCollisionExit(Collision others)
    {
        if (others.gameObject.CompareTag("Ground"))//regarde quand le joueur n'est pas sur le sol.
        {
            IsGrounded = false;
            Debug.Log("est en l'air");
        }
    }
}
