﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructionText : MonoBehaviour {
    public float time = 0;
    public int temps;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		time = time + Time.deltaTime;
        if (time > temps)
        {
            Destroy(gameObject);
        }
	}
}
