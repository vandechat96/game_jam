Les dessins sont actuellement divisés en 2 dossiers

Alterable Drawing : contient tout les dessins en format ".svg" (format vectoriel) et sont modifiables avec inkscape

Definitive Drawing : contient tout les dessins en format moins facilement modifiable ".png" (format matriciel)